
$(window).on("load",function(){
  $(".second-step__packages,.second-step__booking-treatments_editable,.second-step__booking-treatments_ready").mCustomScrollbar({
    theme: "dark",
    axis: "y",
    autoDraggerLength: true
  });
});
$( window ).resize(function() {
  var widthWin = $(window).width();

  var temp = $('.second-step__header-checkout-list').offset().left

  $('.second-step__header-checkout-body').css('width', widthWin - temp + 'px');
});
$(document).ready(function(){

  var widthWin = $(window).width();

  var temp = $('.second-step__header-checkout-list').offset().left

  $('.second-step__header-checkout-body').css('width', widthWin - temp + 'px');
  function booking(){
    var widthWin = $(window).width();

    var temp = $('.second-step__header-checkout-list').offset().left

    $('.second-step__header-checkout-body').css('width', widthWin - temp + 'px');
  }



  $('.first-step__form_input-submit').click(function(e){
    e.preventDefault();
    $('.first-step').css('display', 'none');
    $('.second-step').css('display', 'block');
    booking();
  })
  $('.second-step__main-step_1 .second-step__checkout-step-next').click(function(e){
    $('.second-step__main-step_1').css('display', 'none');
    $('.second-step__main-step_4').css('display', 'block');
    $('.second-step__main-step_4').addClass('second-step__checkout-step_open');
    if( $('.second-step__main-step_4').hasClass('second-step__checkout-step_open')){
      $('.second-step__booking-treatments_editable').css('display', 'none');
      $('.second-step__booking-treatments_ready').css('display', 'block');
      $('.second-step__booking-discount').css('display', 'block');

    }
  });


  $('.second-step__minus').on('click', function (e) {
    e.preventDefault();
    var elemVal = $(this).siblings('input').val() - 1;

    if (elemVal < 0) {
        $(this).siblings('input').val(0);
    } else {
        $(this).siblings('input').val(elemVal);
    }
  });
  $('.second-step__plus').on('click', function (e) {
    e.preventDefault();
    var elemVal = +$(this).siblings('input').val() + 1;

    $(this).siblings('input').val(elemVal);
  });

  $('.second-step__header-checkout-list').click(function(){
    if($('.second-step__header-checkout-body').hasClass('open')){
      $('.second-step__header-checkout-body').toggleClass('open');
      $('.second-step__header-checkout-body').height('0px');
    }else{
      $('.second-step__header-checkout-body').toggleClass('open');
      $('.second-step__header-checkout-body').height('calc(100% - 194px)');
    }

  })
});
